import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test(timeout = 20000)
   public void testEmpty() {
      //Empty graph must return empty pair
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex[] res = g.bfsDistance();
      assertNull(res[0]);
      assertNull(res[1]);
   }

   @Test(timeout = 20000)
   public void testBFS() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[4];
      for (int i = 0; i < 4; ++i) {
         vertices[i] = g.createVertex("v" + String.valueOf(4 - i));
         System.out.println(vertices[i]);
      }

      g.createArc("av1_v2", vertices[3], vertices[2]);
      g.createArc("av2_v1", vertices[2], vertices[3]);

      g.createArc("av3_v1", vertices[1], vertices[3]);
      g.createArc("av1_v3", vertices[3], vertices[1]);

      g.createArc("av4_v3", vertices[0], vertices[1]);
      g.createArc("av3_v4", vertices[1], vertices[0]);

      GraphTask.Vertex[] res = g.bfsDistance();

      assertEquals("v2", res[0].toString());
      assertEquals("v4", res[1].toString());
   }

   @Test(timeout = 20000)
   public void testCycle() {
      //Check if algoritm will crash if there is a cycle
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[3];
      for (int i = 0; i < 3; ++i)
         vertices[i] = g.createVertex("v" + String.valueOf(3 - i));

      g.createArc("av1_v2", vertices[2], vertices[1]);
      g.createArc("av2_v1", vertices[1], vertices[2]);

      g.createArc("av3_v1", vertices[0], vertices[2]);
      g.createArc("av1_v3", vertices[2], vertices[0]);


      g.createArc("av2_v3", vertices[1], vertices[2]);
      g.createArc("av3_v2", vertices[2], vertices[1]);

      GraphTask.Vertex[] res = g.bfsDistance();

      assertEquals("v2", res[0].toString());
      assertEquals("v3", res[1].toString());
   }

   @Test(timeout = 20000)
   public void testBigTree() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.createRandomTree(2500);
      GraphTask.Vertex [] res = g.bfsDistance();
   }

   @Test(timeout = 20000)
   public void testBigGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.createRandomSimpleGraph(2500, 3000);
      GraphTask.Vertex [] res = g.bfsDistance();

   }
}

